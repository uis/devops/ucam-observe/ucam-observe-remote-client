# UCAM Observe Remote Client

The `ucam-observe-remote-client` package is designed to facilitate logging by sending structured log data to a remote server. This package utilizes [Pino](https://github.com/pinojs/pino) for logging and [Axios](https://github.com/axios/axios) for handling HTTP requests.

## Features

- Log messages at various levels: info, error, debug, warning, fatal and critical.
- Send log data asynchronously to a remote server.
- Configurable to ensure minimal impact on application performance.

## Installation

Install the package using npm:

```bash
npm install ucam-observe-remote-client
```

## Usage

To use the logger, you must configure it with the appropriate level and remote URL of your logging backend:

```typescript
import { getPinoRemoteLogger } from 'ucam-observe-remote-client';

const logger = getPinoRemoteLogger({
  level: 'info',
  remoteUrl: 'https://your-log-endpoint.com/logs'
});

// Example log messages
logger.info('This is a test log', { foo: 'bar' });
logger.error('Error encountered', { error: 'Sample error message' });
```

The `setupGlobalErrorHandler` function enhances application robustness by automatically capturing and logging unhandled errors and unhandled promise rejections. This feature uses window event listeners to catch and report runtime errors and promise rejections to your configured remote logging server.

To activate global error logging, import and configure `setupGlobalErrorHandler` from the `ucam-observe-remote-client` package. You will need to specify the logging level and the remote URL where the logs should be sent:

```typescript
import { getPinoRemoteLogger, setupGlobalErrorHandler } from 'ucam-observe-remote-client';

const logger = getPinoRemoteLogger({
  level: 'info',
  remoteUrl: 'https://your-log-endpoint.com/logs'
});

setupGlobalErrorHandler(logger);
```

## Testing

To run the tests, execute the following command:

```bash
npm run test
```

## CI/CD Integration

This package is integrated with GitLab CI/CD, which automates the process of linting, testing, and publishing:

- **Lint and Format**: Ensures code quality by running ESLint and Prettier.
- **Run Tests**: Executes unit tests to verify functionality across multiple scenarios.
- **Publish**: Automatically publishes the package to npm when changes are pushed to the main branch.

Ensure the `NPM_AUTH_TOKEN` is set in the GitLab CI/CD settings to automate the publishing process:

```bash
npm login
Username: {retrieve from the DevOps secrets repo}
Password: {retrieve from the DevOps secrets repo}
Email: devops+npm@uis.cam.ac.uk

npm token create
Password: {as above}
```

## License

This project is licensed under the MIT License.

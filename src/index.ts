import pino from 'pino';
import axios from 'axios';

type LogLevel = 'info' | 'error' | 'debug' | 'warning' | 'fatal' | 'critical' | 'notset';

interface LoggerOptions {
  level: LogLevel;
  remoteUrl: string;
  clientId?: string;
}

const pinoToBackendLogLevelMap: Record<string, LogLevel> = {
  '60': 'critical', // fatal -> critical
  '50': 'error', // error -> error
  '40': 'warning', // warn -> warning
  '30': 'info', // info -> info
  '20': 'debug', // debug -> debug
  '10': 'notset' // trace -> notset
};

const getPinoRemoteLogger = (options: LoggerOptions, axiosInstance = axios) => {
  const logger = pino({
    browser: {
      transmit: {
        level: options.level,
        send: (level, logEvent) => {
          const logLevel = pinoToBackendLogLevelMap[level] || 'info';
          const message =
            typeof logEvent.messages[0] === 'object'
              ? JSON.stringify(logEvent.messages[0].message)
              : logEvent.messages[0];

          const data = {
            messages: logEvent.messages,
            bindings: logEvent.bindings
          };

          axiosInstance
            .post(
              options.remoteUrl,
              {
                log_level: logLevel,
                event: message,
                data: data
              },
              {
                headers: options.clientId ? { 'X-Ucam-Observe-Client-Id': options.clientId } : {}
              }
            )
            .catch((error) => {
              console.error('Error caught in logger:', error.message);
              if (error.response) {
                console.error('Failed to send log:', error.response.status, error.response.data);
              } else if (error.request) {
                console.error('Request made but no response was received');
              } else {
                console.error('Error setting up the request:', error.message);
              }
            });
        }
      }
    }
  });

  return logger;
};

export * from './errorHandler';
export { getPinoRemoteLogger, LoggerOptions, LogLevel, pinoToBackendLogLevelMap };

import { Logger } from 'pino';

const setupGlobalErrorHandler = (logger: Logger) => {
  const initialOnError = window.onerror;

  window.onerror = (message, source, lineno, colno, error) => {
    logger.error({
      message,
      source,
      lineno,
      colno,
      error: error?.stack || error
    });
    return true;
  };

  return () => {
    window.onerror = initialOnError;
  };
};

export { setupGlobalErrorHandler };
